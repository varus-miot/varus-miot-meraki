# VARUS MIoT #



### What is VARUS MIoT? ###

**VARUS MIoT** (**V**arious **A**nalytical **R**esources **U**sed for **S**urveillance – **M**edical **I**nternet of **T**hings) 
is a medical monitoring device that collects, processes, and transmits real-time data and / or video 
from patient diagnostic sensors to hosting devices on the **Cisco Meraki** platform. 